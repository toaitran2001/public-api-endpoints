package vn.com.ari.api.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.sql.Timestamp;
import java.time.Instant;
import java.time.LocalDateTime;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class ParamHisDataDto {
    private String nodeCustomId;
    private String format;
    private Instant startTime;
    private Instant endTime;
    private Integer limit;
    private String aggregate;
}
