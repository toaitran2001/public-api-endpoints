package vn.com.ari.api.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.stereotype.Component;
import vn.com.ari.api.type.Data;
import vn.com.ari.api.type.Header;

import java.util.List;

@Component
@NoArgsConstructor
@Getter
@Setter
public class HistoricDataDto {
    private String docType;

    private Header header;

    private List<Data> data;
}
