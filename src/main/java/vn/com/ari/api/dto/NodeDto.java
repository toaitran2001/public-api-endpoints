package vn.com.ari.api.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.stereotype.Component;

import java.time.Instant;

@Component
@NoArgsConstructor
@Getter
@Setter
public class NodeDto {
    private String customId;

    private Integer id;

    private Instant createdTime;

    private boolean isActive;

    private String name;

    private String ownerId;

    public void setIsActive(boolean isActive) {
        this.isActive = isActive;
    }
}
