package vn.com.ari.api.repo;

import org.springframework.data.cassandra.repository.CassandraRepository;
import org.springframework.data.cassandra.repository.Query;
import org.springframework.stereotype.Repository;
import vn.com.ari.api.model.HistoricData;

import java.time.Instant;
import java.util.List;

@Repository
public interface HistoricDataRepository extends CassandraRepository<HistoricData, HistoricData.Key> {
    @Query("SELECT * FROM historic_data WHERE node_custom_id=?0 and timestamp>=?1 and timestamp<=?2 LIMIT ?3;")
    List<HistoricData> findHistoricData(String nodeCustomId, Instant startTime, Instant endTime, Integer recordCount);
}
