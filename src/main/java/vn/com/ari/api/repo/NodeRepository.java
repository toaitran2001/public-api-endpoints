package vn.com.ari.api.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import vn.com.ari.api.model.Node;

@Repository
public interface NodeRepository extends JpaRepository<Node, String> {
}
