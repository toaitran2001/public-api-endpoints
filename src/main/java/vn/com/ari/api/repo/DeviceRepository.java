package vn.com.ari.api.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import vn.com.ari.api.model.Device;

@Repository
public interface DeviceRepository extends JpaRepository<Device, String> {
}
