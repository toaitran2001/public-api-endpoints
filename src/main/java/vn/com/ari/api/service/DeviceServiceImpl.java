package vn.com.ari.api.service;

import org.springframework.stereotype.Component;
import vn.com.ari.api.dto.DeviceDto;
import vn.com.ari.api.map.DeviceMapper;
import vn.com.ari.api.model.Device;
import vn.com.ari.api.repo.DeviceRepository;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Component
public class DeviceServiceImpl implements DeviceService {
    private DeviceRepository deviceRepository;

    private DeviceMapper deviceMapper;

    public DeviceServiceImpl(DeviceRepository deviceRepository, DeviceMapper deviceMapper) {
        this.deviceRepository = deviceRepository;
        this.deviceMapper = deviceMapper;
    }

    @Override
    public List<DeviceDto> retrieveAllDevices(List<String> customIds) {
        List<DeviceDto> listDeviceDto = deviceRepository.findAllById(customIds).stream().map(deviceMapper::toDeviceDto).collect(Collectors.toList());
        return listDeviceDto;
    }

    @Override
    public DeviceDto retrieveDeviceById(String customId) {
        Optional<Device> device = deviceRepository.findById(customId);
        if (!device.isPresent()) {
            return null;
        }
        return deviceMapper.toDeviceDto(device.get());
    }
}
