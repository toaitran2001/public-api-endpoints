package vn.com.ari.api.service;

import org.springframework.stereotype.Service;
import vn.com.ari.api.model.Authorization;

@Service
public interface AuthorizationService {
    public Authorization retrieveAuthorization(String key);
}
