package vn.com.ari.api.service;

import org.springframework.stereotype.Service;
import vn.com.ari.api.dto.NodeDto;

import java.util.List;

@Service
public interface NodeService {
    public List<NodeDto> retrieveAllNodes(List<String> customIds);
    public NodeDto retrieveNodeById(String customId);
}
