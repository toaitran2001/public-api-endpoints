package vn.com.ari.api.service;

import org.springframework.stereotype.Service;
import vn.com.ari.api.dto.HistoricDataDto;
import vn.com.ari.api.dto.ParamHisDataDto;

@Service
public interface DataService {
    public HistoricDataDto retrieveHistoricData(ParamHisDataDto params);
}
