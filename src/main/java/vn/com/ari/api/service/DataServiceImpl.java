package vn.com.ari.api.service;

import org.springframework.stereotype.Component;
import vn.com.ari.api.dto.HistoricDataDto;
import vn.com.ari.api.dto.ParamHisDataDto;
import vn.com.ari.api.map.HistoricDataMapper;
import vn.com.ari.api.model.HistoricData;
import vn.com.ari.api.repo.HistoricDataRepository;

import java.util.List;

@Component
public class DataServiceImpl implements DataService {
    private HistoricDataRepository historicDataRepository;

    public DataServiceImpl(HistoricDataRepository historicDataRepository) {
        this.historicDataRepository = historicDataRepository;
    }

    @Override
    public HistoricDataDto retrieveHistoricData(ParamHisDataDto params) {
        List<HistoricData> listHistoricData = historicDataRepository.findHistoricData(params.getNodeCustomId(), params.getStartTime(), params.getEndTime(), params.getLimit());
        if (listHistoricData.isEmpty()) {
            return null;
        }
        HistoricDataDto historicDataDto = HistoricDataMapper.toHistoricDataDto(listHistoricData, params.getFormat(), params.getStartTime(), params.getEndTime(), params.getLimit(), params.getAggregate());
        return historicDataDto;
    }
}
