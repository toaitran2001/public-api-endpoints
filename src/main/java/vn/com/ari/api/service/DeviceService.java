package vn.com.ari.api.service;

import org.springframework.stereotype.Service;
import vn.com.ari.api.dto.DeviceDto;

import java.util.List;

@Service
public interface DeviceService {
    public List<DeviceDto> retrieveAllDevices(List<String> customIds);
    public DeviceDto retrieveDeviceById(String customId);
}
