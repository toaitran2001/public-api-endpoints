package vn.com.ari.api.service;

import org.springframework.stereotype.Component;
import vn.com.ari.api.model.Authorization;
import vn.com.ari.api.repo.AuthorizationRepository;

import java.util.Optional;

@Component
public class AuthorizationServiceImpl implements AuthorizationService {
    private AuthorizationRepository authorizationRepository;

    public AuthorizationServiceImpl(AuthorizationRepository authorizationRepository) {
        this.authorizationRepository = authorizationRepository;
    }

    @Override
    public Authorization retrieveAuthorization(String key) {
        Optional<Authorization> authorization = authorizationRepository.findByKey(key);
        if (!authorization.isPresent()) {
            return null;
        }
        return authorization.get();
    }
}
