package vn.com.ari.api.service;

import org.springframework.stereotype.Component;
import vn.com.ari.api.dto.NodeDto;
import vn.com.ari.api.map.NodeMapper;
import vn.com.ari.api.model.Node;
import vn.com.ari.api.repo.NodeRepository;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Component
public class NodeServiceImpl implements NodeService {
    private NodeRepository nodeRepository;

    private NodeMapper nodeMapper;

    public NodeServiceImpl(NodeRepository nodeRepository, NodeMapper nodeMapper) {
        this.nodeRepository = nodeRepository;
        this.nodeMapper = nodeMapper;
    }

    @Override
    public List<NodeDto> retrieveAllNodes(List<String> customIds) {
        List<NodeDto> listNodeDto = nodeRepository.findAllById(customIds).stream().map(nodeMapper::toNodeDto).collect(Collectors.toList());
        return listNodeDto;
    }

    @Override
    public NodeDto retrieveNodeById(String customId) {
        Optional<Node> node = nodeRepository.findById(customId);
        if (!node.isPresent()) {
            return null;
        }
        return nodeMapper.toNodeDto(node.get());
    }
}
