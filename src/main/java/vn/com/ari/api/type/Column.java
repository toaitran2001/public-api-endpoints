package vn.com.ari.api.type;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class Column {
    private Integer id;
    private String name;
    private String dataType;
    private String aggregate;
}
