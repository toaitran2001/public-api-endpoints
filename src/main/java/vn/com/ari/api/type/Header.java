package vn.com.ari.api.type;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.Instant;
import java.util.TreeMap;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class Header {
    private Instant startTime;
    private Instant endTime;
    private Integer recordCount;
    private TreeMap<String, Column> columns;
}
