package vn.com.ari.api.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import vn.com.ari.api.dto.DeviceDto;
import vn.com.ari.api.exception.ExceptionBadRequest;
import vn.com.ari.api.exception.ExceptionNotAuthorized;
import vn.com.ari.api.exception.ExceptionNotFound;
import vn.com.ari.api.model.Authorization;
import vn.com.ari.api.service.AuthorizationService;
import vn.com.ari.api.service.DeviceService;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@RestController
@RequestMapping("/api/v1/devices")
public class DeviceController {
    private AuthorizationService authorizationService;
    private DeviceService deviceService;

    public DeviceController(AuthorizationService authorizationService, DeviceService deviceService) {
        this.authorizationService = authorizationService;
        this.deviceService = deviceService;
    }

    // Check a string which is Positive Integer or not
    public static boolean isPositiveInteger(String str) {
        try {
            Integer intStr = Integer.parseInt(str);
            if (intStr > 0) {
                return true;
            }
            return false;
        } catch (Exception e) {
            return false;
        }
    }

    // Check an id which is in custom id format or not
    public static String parseCustomId(String str) {
        if (str.contains("@") && str.contains("_")) {
            String[] parts = str.split("[@_]");
            if (parts[0].length() == 1 && "WSCP".contains(parts[0]) && DeviceController.isPositiveInteger(parts[2])) {
                return parts[0];
            }
        }
        return null;
    }

    // Check an API key which is authorized or not
    public static boolean isAuthorized(Authorization authorization, String customId, String typeId) {
        if (typeId.equals("W") && authorization.getListWorkspace().contains(customId)) {
            return true;
        } else if (typeId.equals("S") && authorization.getListStation().contains(customId)) {
            return true;
        } else if (typeId.equals("C") && authorization.getListConnection().contains(customId)) {
            return true;
        } else if (typeId.equals("P") && authorization.getListParam().contains(customId)) {
            return true;
        }
        return false;
    }

    // API for retrieving all devices
    @GetMapping("")
    public ResponseEntity<List<DeviceDto>> retrieveAllDevices(@RequestHeader(value = "Authorization") String key) throws Exception {
        // Check API key
        if (key.length() != 16) {
            throw new ExceptionBadRequest("Invalid API key: " + key + ". It must have 16 characters!");
        }
        Authorization authorization = authorizationService.retrieveAuthorization(key);
        if (authorization == null) {
            throw new ExceptionNotFound("API key " + key + " is not found!");
        }

        // Get all custom ids
        List<String> customIds = Stream.of(authorization.getListWorkspace(), authorization.getListStation(), authorization.getListConnection(),authorization.getListParam())
                .flatMap(Collection::stream)
                .collect(Collectors.toList());

        List<DeviceDto> listDeviceDto = deviceService.retrieveAllDevices(customIds);

        return new ResponseEntity<List<DeviceDto>>(listDeviceDto, HttpStatus.OK);
    }

    // API for retrieving a device by id
    @GetMapping("/{id}")
    public ResponseEntity<DeviceDto> retrieveDeviceById(@RequestHeader(value = "Authorization") String key, @PathVariable(value = "id") String customId) throws Exception {
        // Check id which is custom id or not
        String typeId = DeviceController.parseCustomId(customId);
        if (typeId == null) {
            throw new ExceptionBadRequest("Invalid id: " + customId);
        }

        // Check API key
        if (key.length() != 16) {
            throw new ExceptionBadRequest("Invalid API key: " + key + ". It must have 16 characters!");
        }
        Authorization authorization = authorizationService.retrieveAuthorization(key);
        if (authorization == null) {
            throw new ExceptionNotFound("API key " + key + " is not found!");
        } else if (!DeviceController.isAuthorized(authorization, customId, typeId)) {
            throw new ExceptionNotAuthorized("API key " + key + " is not authorized to query id " + customId);
        }

        DeviceDto deviceDto = deviceService.retrieveDeviceById(customId);
        if (deviceDto == null) {
            throw new ExceptionNotFound("Device is not found with id: " + customId);
        }

        return new ResponseEntity<DeviceDto>(deviceDto, HttpStatus.OK);
    }
}
