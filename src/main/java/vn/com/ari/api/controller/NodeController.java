package vn.com.ari.api.controller;

import org.springframework.data.util.Pair;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import vn.com.ari.api.dto.HistoricDataDto;
import vn.com.ari.api.dto.NodeDto;
import vn.com.ari.api.dto.ParamHisDataDto;
import vn.com.ari.api.exception.ExceptionBadRequest;
import vn.com.ari.api.exception.ExceptionMissField;
import vn.com.ari.api.exception.ExceptionNotAuthorized;
import vn.com.ari.api.exception.ExceptionNotFound;
import vn.com.ari.api.model.Authorization;
import vn.com.ari.api.service.AuthorizationService;
import vn.com.ari.api.service.DataService;
import vn.com.ari.api.service.NodeService;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import java.time.Instant;
@RestController
@RequestMapping("/api/v1/nodes")
public class NodeController {
    private AuthorizationService authorizationService;
    private NodeService nodeService;
    private DataService dataService;

    public NodeController(AuthorizationService authorizationService, NodeService nodeService, DataService dataService) {
        this.authorizationService = authorizationService;
        this.nodeService = nodeService;
        this.dataService = dataService;
    }

    // Check a string which is Positive Integer or not
    public static boolean isPositiveInteger(String str) {
        try {
            Integer intStr = Integer.parseInt(str);
            if (intStr > 0) {
                return true;
            }
            return false;
        } catch (Exception e) {
            return false;
        }
    }

    // Check an id which is in custom id format or not
    public static String parseCustomId(String str) {
        if (str.contains("@") && str.contains("_")) {
            String[] parts = str.split("[@_]");
            if (parts[0].length() == 1 && "WSCP".contains(parts[0]) && NodeController.isPositiveInteger(parts[2])) {
                return parts[0];
            }
        }
        return null;
    }

    // Check an API key which is authorized or not
    public static boolean isAuthorized(Authorization authorization, String customId, String typeId) {
        if (typeId.equals("W") && authorization.getListWorkspace().contains(customId)) {
            return true;
        } else if (typeId.equals("S") && authorization.getListStation().contains(customId)) {
            return true;
        } else if (typeId.equals("C") && authorization.getListConnection().contains(customId)) {
            return true;
        } else if (typeId.equals("P") && authorization.getListParam().contains(customId)) {
            return true;
        }
        return false;
    }

    // Check startTime and endTime which are in format or not
    public static Pair<Instant, Instant> toTimeStamp(String startTime, String endTime) {
        try {
            Instant startTimeTs = Instant.parse(startTime);
            Instant endTimeTs = Instant.parse(endTime);
            return Pair.of(startTimeTs, endTimeTs);
        } catch (Exception e) {
            return null;
        }
    }

    // API for retrieving all nodes
    @GetMapping("")
    public ResponseEntity<List<NodeDto>> retrieveAllNodes(@RequestHeader(value = "Authorization") String key) throws Exception {
        // Check API key
        if (key.length() != 16) {
            throw new ExceptionBadRequest("Invalid API key: " + key + ". It must have 16 characters!");
        }
        Authorization authorization = authorizationService.retrieveAuthorization(key);
        if (authorization == null) {
            throw new ExceptionNotFound("API key " + key + " is not found!");
        }

        // Get all custom ids
        List<String> customIds = Stream.of(authorization.getListWorkspace(), authorization.getListStation(), authorization.getListConnection(),authorization.getListParam())
                .flatMap(Collection::stream)
                .collect(Collectors.toList());

        List<NodeDto> listNodeDto = nodeService.retrieveAllNodes(customIds);

        return new ResponseEntity<List<NodeDto>>(listNodeDto, HttpStatus.OK);
    }

    // API for retrieving a node by id
    @GetMapping("/{id}")
    public ResponseEntity<NodeDto> retrieveNodeById(@RequestHeader(value = "Authorization") String key, @PathVariable(value = "id") String customId) throws Exception {
        // Check id which is custom id or not
        String typeId = NodeController.parseCustomId(customId);
        if (typeId == null) {
            throw new ExceptionBadRequest("Invalid id: " + customId);
        }

        // Check API key
        if (key.length() != 16) {
            throw new ExceptionBadRequest("Invalid API key: " + key + ". It must have 16 characters!");
        }
        Authorization authorization = authorizationService.retrieveAuthorization(key);
        if (authorization == null) {
            throw new ExceptionNotFound("API key " + key + " is not found!");
        } else if (!NodeController.isAuthorized(authorization, customId, typeId)) {
            throw new ExceptionNotAuthorized("API key " + key + " is not authorized to query id " + customId);
        }

        NodeDto nodeDto = nodeService.retrieveNodeById(customId);
        if (nodeDto == null) {
            throw new ExceptionNotFound("Node is not found with id: " + customId);
        }

        return new ResponseEntity<NodeDto>(nodeDto, HttpStatus.OK);
    }

    // API for retrieving node historic data
    @GetMapping("/{id}/historic")
    public ResponseEntity<?> retrieveHistoricData (
            @RequestHeader(value = "Authorization") String key,
            @PathVariable(value = "id") String customId,
            @RequestParam(value = "format", required = false, defaultValue = "json") String format,
            @RequestParam(value = "startTime", required = true, defaultValue = "") String startTime,
            @RequestParam(value = "endTime", required = true, defaultValue = "") String endTime,
            @RequestParam(value = "limit", required = false, defaultValue = "10") Integer limit,
            @RequestParam(value = "aggregate", required = false, defaultValue = "NONE") String aggregate) throws Exception {

        if (startTime.equals("") || endTime.equals("")) {
            throw new ExceptionMissField("Missing field in params!");
        }

        // Convert startTime and endTime to time stamp type
        Pair<Instant, Instant> pairTime = NodeController.toTimeStamp(startTime, endTime);
        if (pairTime == null) {
            throw new ExceptionBadRequest("Invalid time!");
        }
        Instant startTimeTs = pairTime.getFirst();
        Instant endTimeTs = pairTime.getSecond();

        if (startTimeTs.isAfter(endTimeTs)) {
            throw new ExceptionBadRequest("Start time must not be after end time!");
        }

        // Check id which is custom id or not
        String typeId = NodeController.parseCustomId(customId);
        if (typeId == null) {
            throw new ExceptionBadRequest("Invalid id: " + customId);
        }

        // Check API key
        if (key.length() != 16) {
            throw new ExceptionBadRequest("Invalid API key: " + key + ". It must have 16 characters!");
        }
        Authorization authorization = authorizationService.retrieveAuthorization(key);
        if (authorization == null) {
            throw new ExceptionNotFound("API key " + key + " is not found!");
        } else if (!NodeController.isAuthorized(authorization, customId, typeId)) {
            throw new ExceptionNotAuthorized("API key " + key + " is not authorized to query id " + customId);
        }

        // Parameters
        ParamHisDataDto params = new ParamHisDataDto(customId, format, startTimeTs, endTimeTs, limit, aggregate);

        HistoricDataDto historicData = dataService.retrieveHistoricData(params);
        if (historicData == null) {
            throw new ExceptionNotFound("Node historic data is not found with id: " + customId);
        }

        return new ResponseEntity<HistoricDataDto>(historicData, HttpStatus.OK);
    }
}
