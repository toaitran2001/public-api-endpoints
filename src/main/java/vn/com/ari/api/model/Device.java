package vn.com.ari.api.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.time.Instant;

@Entity
@Table(name = "all_devices")
@AllArgsConstructor
@NoArgsConstructor
@Data
public class Device {
    @Id
    @Column(name = "custom_id")
    private String customId;

    @Column(name = "id")
    private Integer id;

    @Column(name = "created_time")
    private Instant createdTime;

    @Column(name = "is_active")
    private boolean isActive;

    @Column(name = "name")
    private String name;

    @Column(name = "owner_id")
    private String ownerId;

    @Column(name = "is_connected")
    private boolean isConnected;

    public boolean getIsActive() {
        return this.isActive;
    }

    public boolean getIsConnected() { return this.isConnected;}
}
