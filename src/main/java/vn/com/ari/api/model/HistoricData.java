package vn.com.ari.api.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.cassandra.core.cql.PrimaryKeyType;
import org.springframework.data.cassandra.core.mapping.*;

import java.io.Serializable;
import java.time.Instant;

@Table("historic_data")
@AllArgsConstructor
@NoArgsConstructor
@Data
public class HistoricData {
    @PrimaryKeyClass
    @AllArgsConstructor
    @Data
    public static class Key implements Serializable {
        @PrimaryKeyColumn(name = "node_custom_id", ordinal = 0, type = PrimaryKeyType.PARTITIONED)
        private String nodeCustomId;
        @PrimaryKeyColumn(name = "timestamp", ordinal = 1, type = PrimaryKeyType.CLUSTERED)
        private Instant timestamp;
        @PrimaryKeyColumn(name = "column_id", ordinal = 2, type = PrimaryKeyType.CLUSTERED)
        private Integer columnId;
    }

    @PrimaryKey
    HistoricData.Key primaryKey;

    @Column("node_id")
    private Integer nodeId;

    @Column("column_name")
    private String columnName;

    @Column("value")
    private Object columnValue;
}
