package vn.com.ari.api.model;

import com.vladmihalcea.hibernate.type.array.ListArrayType;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "api_keys")
@TypeDef(name = "list-array", typeClass = ListArrayType.class)
@AllArgsConstructor
@NoArgsConstructor
@Data
public class Authorization {
    @Id
    @Column(name = "api_key")
    private String key;

    @Type(type = "list-array")
    @Column(name = "workspaces", columnDefinition = "integer[]")
    private List<String> listWorkspace;

    @Type(type = "list-array")
    @Column(name = "stations", columnDefinition = "integer[]")
    private List<String> listStation;

    @Type(type = "list-array")
    @Column(name = "connections", columnDefinition = "integer[]")
    private List<String> listConnection;

    @Type(type = "list-array")
    @Column(name = "params", columnDefinition = "integer[]")
    private List<String> listParam;
}
