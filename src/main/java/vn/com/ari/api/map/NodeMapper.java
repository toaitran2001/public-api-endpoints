package vn.com.ari.api.map;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import vn.com.ari.api.dto.NodeDto;
import vn.com.ari.api.model.Node;

@Mapper(componentModel = "spring")
public interface NodeMapper {
    @Mapping(target = "id", source = "node.id")
    @Mapping(target = "createdTime", source = "node.createdTime")
    @Mapping(target = "isActive", source = "node.isActive")
    @Mapping(target = "name", source = "node.name")
    @Mapping(target = "ownerId", source = "node.ownerId")
    @Mapping(target = "customId", source = "node.customId")
    NodeDto toNodeDto(Node node);
}
