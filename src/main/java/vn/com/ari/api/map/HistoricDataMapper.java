package vn.com.ari.api.map;

import vn.com.ari.api.dto.*;
import vn.com.ari.api.model.HistoricData;
import vn.com.ari.api.type.Cell;
import vn.com.ari.api.type.Column;
import vn.com.ari.api.type.Data;
import vn.com.ari.api.type.Header;

import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.TreeMap;

public class HistoricDataMapper {
    public static String getDataType(Object v) {
        if (v.getClass() == String.class) {
            return "TEXT";
        }
        return "NUMBER";
    }

    public static HistoricDataDto toHistoricDataDto(List<HistoricData> listHistoricData, String format, Instant startTime, Instant endTime, Integer limit, String aggregate) {
        // Declare variable to store information of all columns
        TreeMap<String, Column> columns = new TreeMap<>();
        // Declare variable to store all columns names and their index
        TreeMap<String, String> dictColumnName = new TreeMap<>();
        // Declare variable to store index of current column
        int countColumn = 0;
        // Declare variable to store all data of all columns
        List<Data> listData = new ArrayList<Data>();

        for (HistoricData historicData : listHistoricData) {
            String currentColumnName = historicData.getColumnName();
            if (!dictColumnName.containsKey(currentColumnName)) {
                String dataType = HistoricDataMapper.getDataType(historicData.getColumnValue());
                dictColumnName.put(currentColumnName, String.valueOf(countColumn));
                columns.put(String.valueOf(countColumn), new Column(historicData.getPrimaryKey().getColumnId(), currentColumnName, dataType, aggregate));
                countColumn++;
            }

            // Declare variable to store value of each row of each column type
            TreeMap<String, Cell> f = new TreeMap<>();
            f.put(dictColumnName.get(currentColumnName), new Cell(historicData.getColumnValue()));
            listData.add(new Data(historicData.getPrimaryKey().getTimestamp(), f));
        }

        Header tempHeader = new Header(startTime, endTime, limit, columns);

        // Create HistoricDataDto
        HistoricDataDto temp = new HistoricDataDto();
        temp.setDocType(format);
        temp.setHeader(tempHeader);
        temp.setData(listData);
        return temp;
    }
}
