package vn.com.ari.api.map;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import vn.com.ari.api.dto.DeviceDto;
import vn.com.ari.api.model.Device;

@Mapper(componentModel = "spring")
public interface DeviceMapper {
    @Mapping(target = "id", source = "device.id")
    @Mapping(target = "createdTime", source = "device.createdTime")
    @Mapping(target = "isActive", source = "device.isActive")
    @Mapping(target = "name", source = "device.name")
    @Mapping(target = "ownerId", source = "device.ownerId")
    @Mapping(target = "customId", source = "device.customId")
    @Mapping(target = "isConnected", source = "device.isConnected")
    DeviceDto toDeviceDto(Device device);
}
