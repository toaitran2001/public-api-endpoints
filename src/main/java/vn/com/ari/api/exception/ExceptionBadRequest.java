package vn.com.ari.api.exception;

public class ExceptionBadRequest extends RuntimeException {
    private static final long serialVersionUID = 1L;
    public ExceptionBadRequest(String message) {
        super(message);
    }
}
