package vn.com.ari.api.exception;

public class ExceptionNotAuthorized extends RuntimeException {
    private static final long serialVersionUID = 1L;
    public ExceptionNotAuthorized(String message) {
        super(message);
    }
}
