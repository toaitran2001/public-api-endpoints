package vn.com.ari.api.exception;

public class ExceptionMissField extends RuntimeException {
    private static final long serialVersionUID = 1L;
    public ExceptionMissField(String message) {
        super(message);
    }
}
