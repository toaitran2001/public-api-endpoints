package vn.com.ari.api.exception;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import vn.com.ari.api.response.ApiResponse;

@RestControllerAdvice
public class CustomExceptionHandler {
    // Handle specific exceptions
    @ExceptionHandler(ExceptionMissField.class)
    public ResponseEntity<ApiResponse> handleExceptionMissField(ExceptionMissField e) {
        ApiResponse apiResponse = new ApiResponse(101, e.getMessage());
        return new ResponseEntity<ApiResponse>(apiResponse, HttpStatus.OK);
    }

    @ExceptionHandler(ExceptionNotAuthorized.class)
    public ResponseEntity<ApiResponse> handleExceptionNotAuthorized(ExceptionNotAuthorized e) {
        ApiResponse apiResponse = new ApiResponse(102, e.getMessage());
        return new ResponseEntity<ApiResponse>(apiResponse, HttpStatus.OK);
    }

    @ExceptionHandler(ExceptionBadRequest.class)
    public ResponseEntity<ApiResponse> handleExceptionBadRequest(ExceptionBadRequest e) {
        ApiResponse apiResponse = new ApiResponse(103, e.getMessage());
        return new ResponseEntity<ApiResponse>(apiResponse, HttpStatus.OK);
    }

    @ExceptionHandler(ExceptionNotFound.class)
    public ResponseEntity<ApiResponse> handleExceptionNotFound(ExceptionNotFound e) {
        ApiResponse apiResponse = new ApiResponse(104, e.getMessage());
        return new ResponseEntity<ApiResponse>(apiResponse, HttpStatus.OK);
    }

    // Handle global exception
    @ExceptionHandler(Exception.class)
    public ResponseEntity<ApiResponse> handleGlobalException(Exception e) {
        return new ResponseEntity<ApiResponse>(ApiResponse.getServerError(), HttpStatus.OK);
    }
}
